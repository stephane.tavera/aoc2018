(ns aoc2018.day3-test
  (:require
    [clojure.test :refer :all]
    [aoc2018.day3 :refer :all]))


(deftest test-parse-line
  (is (= {:id "#282" :x 911 :y 819 :w 17 :h 19}
         (parse-line "#282 @ 911,819: 17x19")))
  (is (= {:id "#1" :x 1 :y 3 :w 4 :h 4}
         (parse-line "#1 @ 1,3: 4x4"))))


