(ns aoc2018.day10-test
  (:require
    [clojure.test :refer :all]
    [aoc2018.file-util :as fu]
    [aoc2018.day10 :refer :all]))

(deftest test-parse-line
  (is (= {:x  7 :y  0 :dx -1 :dy 0}
         (parse-line "position=< 7,  0> velocity=<-1,  0>"))))

(deftest test-parse-line-for-simple-input
  (->> (fu/read-strings "resources/input-day10-simple.txt")
       (map parse-line)
       (run! println)))

(deftest test-move
  (is (= {:x -4 :y 0 :dx 2 :dy 0}
         (move {:x -6, :y 0, :dx 2, :dy 0}))))

(deftest test-bounds
  (is (= {:max-x 15
          :max-y 11
          :min-x -6
          :min-y -4}
         (->>
              (fu/read-strings "resources/input-day10-simple.txt")
              (map parse-line)
              bounds))))

