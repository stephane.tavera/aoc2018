(ns aoc2018.day1-test
  (:require
    [clojure.test :refer :all]
    [aoc2018.day1 :refer :all]))

(deftest test-sol
  (is (= 585 (sol))))

(deftest test-sum-until-repeat
  (is (= 0 (sum-until-repeat [1 -1])))
  (is (= 10 (sum-until-repeat [3 3 4 -2 -4])))
  (is (= 5 (sum-until-repeat [-6 3 8 5 -6])))
  (is (= 14 (sum-until-repeat [+7, +7, -2, -7, -4])))
  )

(deftest test-1
  (is (= 14 (sum-until-repeat [+7, +7, -2, -7, -4]))))

(deftest test-sol2
  (is (= 83173 (sol2))))

