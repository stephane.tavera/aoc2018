(ns aoc2018.day2-test
  (:require
    [clojure.test :refer :all]
    [aoc2018.day2 :refer :all]))

(deftest test-exactly-n-repeats?
  (is (not (exactly-two? "abcdef")))
  (is (not (exactly-three? "abcdef")))

  (is (exactly-two? "bababc"))
  (is (exactly-three? "bababc"))

  (is (exactly-two? "abbcde"))
  (is (not (exactly-three? "abbcde")))

  (is (not (exactly-two? "abcccd")))
  (is (exactly-three? "abcccd"))

  (is (exactly-two? "aabcdd"))
  (is (not (exactly-three? "aabcdd"))))

(deftest test-checksum
  (is (= 12 (checksum ["abcdef" "bababc" "abbcde" "abcccd" "aabcdd" "abcdee" "ababab"]))))

(deftest test-sol1
  (is (= 5368 (sol1))))

(deftest test-common
  (is (nil? (common-dist-1 "abcde" "axcye")))
  (is (= "fgij" (common-dist-1 "fghij" "fguij"))))

(deftest test-sol2
  (is (= "cvgywxqubnuaefmsljdrpfzyi" (sol2))))