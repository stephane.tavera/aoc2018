(ns aoc2018.day10
  (:require
    [clojure.string :as s]))

(defn nth->int
  [coll n]
  (-> (nth coll n) s/trim read-string))

(defn parse-line
  [line]
  (let [tokens (s/split line #"[<>,]")]
    {:x  (nth->int tokens 1)
     :y  (nth->int tokens 2)
     :dx (nth->int tokens 4)
     :dy (nth->int tokens 5)}))

(defn move
  [pt]
  (-> pt
      (update :x #(+ % (:dx pt)))
      (update :y #(+ % (:dy pt)))))

(defn bounds
  [pts]
  (let [xs (map :x pts)
        ys (map :y pts)]
    {:min-x (apply min xs)
     :max-x (apply max xs)
     :min-y (apply min ys)
     :max-y (apply max ys)}))

(defn store-in-map
  [pts]
  (reduce #(assoc %1 [(:x %2) (:y %2)] :ok) {} pts))

(defn dist
  [bounds]
  (max (- (:max-x bounds) (:min-x bounds))
       (- (:max-y bounds) (:min-y bounds))))

(defn draw-pts
  [pts]
  (let [bounds (bounds pts)
        dist (dist bounds)]
    (when (< dist 365)
      (let [{:keys [min-x min-y max-x max-y]} bounds
            map-pts (store-in-map pts)]
        (doseq [j (range min-y (inc max-y))]
          (doseq [i (range min-x (inc max-x))]
            (print (if (map-pts [i j]) "X " "  ")))
          (println))))))

(defn draw-successive
  [n pts]
  (loop [i n pts pts]
    (when (< 0 i)
      (println (- n i))
      (draw-pts pts)
      (recur (dec i) (map move pts)))))

;; Check on screen ;-)
;; (->>
;;   (fu/read-strings "resources/input-day10.txt")
;;   (map parse-line)
;;   (draw-successive 20000))
;;
