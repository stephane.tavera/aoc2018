(ns aoc2018.day5
  (:require
    [aoc2018.file-util :as fu]))

(defonce input
         (->> "resources/input-day5.txt"
              fu/read-strings
              first))

(defn opposite?
  [c1 c2]
  (#{-32 32} (apply - (map int [c1 c2]))))

(defn equal-or-opposite?
  [c1 c2]
  (or (= c1 c2)
      (opposite? c1 c2)))

(defn cons-if-first
  [xs ys]
  (if-let [f (first xs)]
    (cons f ys)
    ys))

(defn fully-react
  [s]
  (loop [seen '() s s]
    (let [a (first s)
          remain (next s)
          b (first remain)
          after-remain (next remain)]
      (if-not b
        (apply str (reverse (cons a seen)))
        (if (opposite? a b)
          (recur (next seen) (cons-if-first seen after-remain))
          (recur (cons a seen) remain))))))

(defn count-fully-react
  [s]
  (count (fully-react s)))

(defn sol
  []
  (count-fully-react input))

(defonce letters (map char (range 65 (+ 65 26))))

(defn remove-letter
  [s c]
  (remove #(equal-or-opposite? c %) s))

(defn sol2
  []
  (->> letters
       (map #(-> input (remove-letter %) count-fully-react))
       (apply min)))
