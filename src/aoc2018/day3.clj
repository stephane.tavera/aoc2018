(ns aoc2018.day3
  (:require
    [clojure.string :as s]
    [aoc2018.file-util :as fu]))

(defn numbers-in-s
  [s sep]
  (map read-string (s/split s sep)))

(defn parse-line
  [s]
  (let [tokens (s/split s #"[ :]")
        [x y] (numbers-in-s (nth tokens 2) #",")
        [w h] (numbers-in-s (nth tokens 4) #"x")]
    {:id (first tokens)
     :x x
     :y y
     :w w
     :h h}))

(defn house
  [house-def]
  (into {}
       (for [i (range (:w house-def))
             j (range (:h house-def))]
         [{:x (+ (:x house-def) i) :y (+ (:y house-def) j)}
          [(:id house-def)]])))

(defn ids-per-coord
  [lines]
  (->> lines
       (map parse-line)
       (map house)
       (apply merge-with concat)))

(defn sol
  []
  (->> (ids-per-coord (fu/read-strings "resources/input-day3.txt"))
       (filter (fn[[k v]] (< 1 (count v))))
       count))

(defn sol2
  []
  (let [lines (fu/read-strings "resources/input-day3.txt")
        ids-per-coord (ids-per-coord lines)
        ids (vals ids-per-coord)
        all-ids (set (map #(str "#" (inc %)) (range (count lines))))
        to-remove (->> ids
                       (filter #(< 1 (count %)))
                       (map set)
                       (apply clojure.set/union))]
    (clojure.set/difference all-ids to-remove)))