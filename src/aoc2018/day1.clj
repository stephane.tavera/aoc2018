(ns aoc2018.day1
  (:require
    [clojure.string :as s]))

(defn read-vals
  []
  (with-open [rdr (clojure.java.io/reader "resources/input.txt")]
    (->> (line-seq rdr)
         (mapv #(-> % s/trim read-string)))))

(defn sol
  []
  (apply + (read-vals)))

(defn sum-until-repeat
  [xs]
  (loop [last-rest 0
         previous-res #{0}
         repeat-xs (cycle xs)]
    (let [x (first repeat-xs)
          new-res (+ last-rest x)]
      (if (previous-res new-res)
        new-res
        (recur new-res
               (conj previous-res new-res)
               (next repeat-xs))))))

(defn sol2
  []
  (sum-until-repeat (read-vals)))