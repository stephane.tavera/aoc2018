(ns aoc2018.file-util)

(defn read-strings
  [path]
  (with-open [rdr (clojure.java.io/reader path)]
    (->> (line-seq rdr)
         (doall))))
