(ns aoc2018.day2
  (:require
    [aoc2018.file-util :as fu]))

(defn exactly-n-repeats?
  [n s]
  (some (fn[[_ v]] (= n v)) (frequencies s)))

(def exactly-two? (partial exactly-n-repeats? 2))
(def exactly-three? (partial exactly-n-repeats? 3))

(defn nb-fulfilling
  [xs p]
  (->> xs
       (filter p)
       count))

(defn checksum
  [ids]
  (* (nb-fulfilling ids exactly-two?) (nb-fulfilling ids exactly-three?)))

(defn sol1
  []
  (checksum (fu/read-strings "resources/input-day2.txt")))

(defn common-dist-1
  [s1 s2]
  (let [common (map #(#{%1} %2) s1 s2)
        dist (count (filter nil? common))]
    (when (= 1 dist)
      (->> common
           (remove nil?)
           (apply str)))))

(defn sol2
  []
  (loop [[id & other-ids] (fu/read-strings "resources/input-day2.txt")]
    (if-let [res (some (fn [other-id] (common-dist-1 id other-id)) other-ids)]
      res
      (recur other-ids))))