(defproject aoc2018 "0.1.0-SNAPSHOT"
  :description "Advent of Code 2018 in Clojure"
  :url "https://adventofcode.com/2018"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies
    [[org.clojure/clojure "1.8.0"]])
